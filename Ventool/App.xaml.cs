﻿using System.Windows;

namespace EnvSafe.Open.EPE
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {
        public static string Version;
    }
}
