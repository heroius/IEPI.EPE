﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EnvSafe.Open.EPE
{
    /// <summary>
    /// InputDust.xaml 的交互逻辑
    /// </summary>
    public partial class InputDust : UserControl
    {
        public InputDust()
        {
            InitializeComponent();
        }

        private void BtnFetchPmax_Click(object sender, RoutedEventArgs e)
        {
            //todo: fetch dust exp data from server
            Process.Start("http://dees.iepi.com.cn/Explosibility/Query.aspx");
        }

        private void BtnFetchKst_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("http://dees.iepi.com.cn/Explosibility/Query.aspx");
        }
    }
}
