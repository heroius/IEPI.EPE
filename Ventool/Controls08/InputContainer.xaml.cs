﻿using EnvSafe.Open.EPE.Vent.GB15605_2008;
using System.Windows;
using System.Windows.Controls;

namespace EnvSafe.Open.EPE
{
    /// <summary>
    /// InputContainer.xaml 的交互逻辑
    /// </summary>
    public partial class InputContainer : UserControl
    {
        public InputContainer()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 辅助计算有效容积和长径比
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Calc_Click(object sender, RoutedEventArgs e)
        {
            DiaEffectVol dia = new DiaEffectVol();
            if (dia.ShowDialog() == true)
            {
                var ceo = (sender as FrameworkElement).DataContext as ContainerEntity;
                ceo.L2DE = dia.L2DE;
                ceo.V = dia.Veff;
                ceo.RaiseAllPropertiesChangedEvent();
            }
        }
        
        private void Feed_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var ceo = (sender as FrameworkElement).DataContext as ContainerEntity;
            ceo.RaiseAllPropertiesChangedEvent();
        }

        private void BtnFetchStep(object sender, RoutedEventArgs e)
        {
            var ceo = (sender as FrameworkElement).DataContext as ContainerEntity;
            ceo.AutoSetDuctStep();
        }
    }
}
