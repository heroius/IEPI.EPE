﻿using EnvSafe.Open.EPE.Vent.EffectVol;
using Heroius.Extension;

/*
 * 关于进一步封装类型的必要性声明
 * 在Heroius.Files.EntitySilo中，递归复杂类型由于技术性制约，无法自动识别接口成员的实际类型
 * 因此对于包含接口属性的各个IContainer具体类，在保存和加载时，截面信息会被丢失
 * 对这些可配置的具体类进一步封装，提供额外的属性访问接口成员，可以避免信息的丢失
 */

namespace EnvSafe.Open.EPE
{
    /// <summary>
    /// 封装<see cref="EnvSafe.Open.EPE.Vent.EffectVol.CommonContainer"/>，圆形截面
    /// </summary>
    public class CylinderContainer : CommonContainer, INotifyAllPropertiesChanged
    {
        public CylinderContainer() : base(SectionType.Circle) { }

        public double BodyDiameter
        {
            get { return (BodySection as CircleSection).Diameter; }
            set { (BodySection as CircleSection).Diameter = value; }
        }

        void INotifyAllPropertiesChanged.RaiseAllPropertiesChangedEvent()
        {
            base.RaiseAllPropertiesChangedEvent();
            RaisePropertyChangedEvent("BodyDiameter");
        }
    }
    /// <summary>
    /// 封装<see cref="EnvSafe.Open.EPE.Vent.EffectVol.CommonContainer"/>，矩形截面
    /// </summary>
    public class BoxContainer : CommonContainer, INotifyAllPropertiesChanged
    {
        public BoxContainer() : base(SectionType.Square) { }

        public double BodyLength
        {
            get { return (BodySection as SquareSection).Length; }
            set { (BodySection as SquareSection).Length = value; }
        }

        public double BodyWidth
        {
            get { return (BodySection as SquareSection).Width; }
            set { (BodySection as SquareSection).Width = value; }
        }

        void INotifyAllPropertiesChanged.RaiseAllPropertiesChangedEvent()
        {
            base.RaiseAllPropertiesChangedEvent();
            RaisePropertyChangedEvent("BodyLength");
            RaisePropertyChangedEvent("BodyWidth");
        }
    }
    /// <summary>
    /// 封装<see cref="EnvSafe.Open.EPE.Vent.EffectVol.HopperContainer"/>，圆形主体截面
    /// </summary>
    public class CylinderHopper : HopperContainer, INotifyAllPropertiesChanged
    {
        public CylinderHopper() : base(SectionType.Circle, SectionType.Circle) { }

        public double BodyDiameter
        {
            get { return (BodySection as CircleSection).Diameter; }
            set { (BodySection as CircleSection).Diameter = value; }
        }

        public double HopperDiameter {
            get { return (HopperLowSection as CircleSection).Diameter; }
            set { (HopperLowSection as CircleSection).Diameter = value; }
        }

        void INotifyAllPropertiesChanged.RaiseAllPropertiesChangedEvent()
        {
            base.RaiseAllPropertiesChangedEvent();
            RaisePropertyChangedEvent("BodyDiameter");
            RaisePropertyChangedEvent("HopperDiameter");
        }
    }
    /// <summary>
    /// 封装<see cref="EnvSafe.Open.EPE.Vent.EffectVol.HopperContainer"/>，矩形主体截面，圆形斗口截面
    /// </summary>
    public class BoxCircleHopper : HopperContainer, INotifyAllPropertiesChanged
    {
        public BoxCircleHopper() : base(SectionType.Square, SectionType.Circle) { }

        public double BodyLength
        {
            get { return (BodySection as SquareSection).Length; }
            set { (BodySection as SquareSection).Length = value; }
        }

        public double BodyWidth
        {
            get { return (BodySection as SquareSection).Width; }
            set { (BodySection as SquareSection).Width = value; }
        }

        public double HopperDiameter
        {
            get { return (HopperLowSection as CircleSection).Diameter; }
            set { (HopperLowSection as CircleSection).Diameter = value; }
        }

        void INotifyAllPropertiesChanged.RaiseAllPropertiesChangedEvent()
        {
            base.RaiseAllPropertiesChangedEvent();
            RaisePropertyChangedEvent("BodyLength");
            RaisePropertyChangedEvent("BodyWidth");
            RaisePropertyChangedEvent("HopperDiameter");
        }
    }
    /// <summary>
    /// 封装<see cref="EnvSafe.Open.EPE.Vent.EffectVol.HopperContainer"/>，矩形主体截面，矩形斗口截面
    /// </summary>
    public class BoxSquareHopper : HopperContainer, INotifyAllPropertiesChanged
    {
        public BoxSquareHopper():base(SectionType.Square, SectionType.Square) { }

        public double BodyLength
        {
            get { return (BodySection as SquareSection).Length; }
            set { (BodySection as SquareSection).Length = value; }
        }

        public double BodyWidth
        {
            get { return (BodySection as SquareSection).Width; }
            set { (BodySection as SquareSection).Width = value; }
        }

        public double HopperLength {
            get { return (HopperLowSection as SquareSection).Length; }
            set { (HopperLowSection as SquareSection).Length = value; }
        }
        public double HopperWidth
        {
            get { return (HopperLowSection as SquareSection).Width; }
            set { (HopperLowSection as SquareSection).Width = value; }
        }

        void INotifyAllPropertiesChanged.RaiseAllPropertiesChangedEvent()
        {
            base.RaiseAllPropertiesChangedEvent();
            RaisePropertyChangedEvent("BodyLength");
            RaisePropertyChangedEvent("BodyWidth");
            RaisePropertyChangedEvent("HopperLength");
            RaisePropertyChangedEvent("HopperWidth");
        }
    }
    /// <summary>
    /// 封装<see cref="EnvSafe.Open.EPE.Vent.EffectVol.VerticalBagFilter"/>，圆形主体截面
    /// </summary>
    public class CylinderBagFilter: VerticalBagFilter, INotifyAllPropertiesChanged
    {
        public CylinderBagFilter():base( SectionType.Circle, SectionType.Circle) { }

        public double BodyDiameter
        {
            get { return (BodySection as CircleSection).Diameter; }
            set { (BodySection as CircleSection).Diameter = value; }
        }

        public double HopperDiameter
        {
            get { return (HopperLowSection as CircleSection).Diameter; }
            set { (HopperLowSection as CircleSection).Diameter = value; }
        }

        void INotifyAllPropertiesChanged.RaiseAllPropertiesChangedEvent()
        {
            base.RaiseAllPropertiesChangedEvent();
            RaisePropertyChangedEvent("BodyDiameter");
            RaisePropertyChangedEvent("HopperDiameter");
        }
    }
    /// <summary>
    /// 封装<see cref="EnvSafe.Open.EPE.Vent.EffectVol.VerticalBagFilter"/>，矩形主体截面，圆形斗口截面
    /// </summary>
    public class BoxCircleBagFilter: VerticalBagFilter, INotifyAllPropertiesChanged
    {
        public BoxCircleBagFilter():base(SectionType.Square, SectionType.Circle) { }

        public double BodyLength
        {
            get { return (BodySection as SquareSection).Length; }
            set { (BodySection as SquareSection).Length = value; }
        }

        public double BodyWidth
        {
            get { return (BodySection as SquareSection).Width; }
            set { (BodySection as SquareSection).Width = value; }
        }

        public double HopperDiameter
        {
            get { return (HopperLowSection as CircleSection).Diameter; }
            set { (HopperLowSection as CircleSection).Diameter = value; }
        }

        void INotifyAllPropertiesChanged.RaiseAllPropertiesChangedEvent()
        {
            base.RaiseAllPropertiesChangedEvent();
            RaisePropertyChangedEvent("BodyLength");
            RaisePropertyChangedEvent("BodyWidth");
            RaisePropertyChangedEvent("HopperDiameter");
        }
    }

    /// <summary>
    /// 封装<see cref="EnvSafe.Open.EPE.Vent.EffectVol.VerticalBagFilter"/>，矩形主体截面，矩形斗口截面
    /// </summary>
    public class BoxSquareBagFilter : VerticalBagFilter, INotifyAllPropertiesChanged
    {
        public BoxSquareBagFilter() : base(SectionType.Square, SectionType.Square) { }

        public double BodyLength
        {
            get { return (BodySection as SquareSection).Length; }
            set { (BodySection as SquareSection).Length = value; }
        }

        public double BodyWidth
        {
            get { return (BodySection as SquareSection).Width; }
            set { (BodySection as SquareSection).Width = value; }
        }

        public double HopperLength
        {
            get { return (HopperLowSection as SquareSection).Length; }
            set { (HopperLowSection as SquareSection).Length = value; }
        }
        public double HopperWidth
        {
            get { return (HopperLowSection as SquareSection).Width; }
            set { (HopperLowSection as SquareSection).Width = value; }
        }

        void INotifyAllPropertiesChanged.RaiseAllPropertiesChangedEvent()
        {
            base.RaiseAllPropertiesChangedEvent();
            RaisePropertyChangedEvent("BodyLength");
            RaisePropertyChangedEvent("BodyWidth");
            RaisePropertyChangedEvent("HopperLength");
            RaisePropertyChangedEvent("HopperWidth");
        }
    }

}
