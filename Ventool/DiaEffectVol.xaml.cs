﻿using Heroius.Extension;
using EnvSafe.Open.EPE.Vent.EffectVol;
using Microsoft.Win32;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace EnvSafe.Open.EPE
{
    /// <summary>
    /// DiaEffectVol.xaml 的交互逻辑
    /// </summary>
    public partial class DiaEffectVol : Window
    {
        public DiaEffectVol()
        {
            InitializeComponent();
            DataContext = EVContext;

            diaopen = new OpenFileDialog();
            diaopen.Filter = "有效容积计算配置|*.evcfg";
            diasave = new SaveFileDialog();
            diasave.Filter = "有效容积计算配置|*.evcfg";

        }

        /// <summary>
        /// 上下文
        /// </summary>
        public EffectVolContext EVContext { get { return EffectVolContext.Context; } }

        /// <summary>
        /// 有效长径比
        /// </summary>
        public double L2DE { get; set; }
        /// <summary>
        /// 有效容积
        /// </summary>
        public double Veff { get; set; }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            L2DE = EVContext.GetSelectedContainer().GetEffectL2D();
            Veff = EVContext.GetSelectedContainer().GetEffectVolumn();
            DialogResult = true;
            Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        /// <summary>
        /// 利用按钮标记聚焦到编辑框
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditFocus_Click(object sender, RoutedEventArgs e)
        {
            // 强制焦点转移
            FocusManager.SetFocusedElement(this, (FindName((sender as Button).Tag as string) as FrameworkElement));
        }

        OpenFileDialog diaopen;
        private void Import_Click(object sender, RoutedEventArgs e)
        {
            if (diaopen.ShowDialog() == true)
            {
                //read from file
                Heroius.Files.EntitySilo silo = new Heroius.Files.EntitySilo();
                silo.Load(diaopen.FileName);
                silo.Fetch("host", EVContext);
                //递归silo属性，注意，此语句位置敏感，必须在host操作之后
                silo.Recursion = true;
                var c = EVContext.GetSelectedContainer();
                silo.Fetch("container", c);
                //update binding targets
                (c as INotifyAllPropertiesChanged).RaiseAllPropertiesChangedEvent();
            }
        }

        SaveFileDialog diasave;

        private void Export_Click(object sender, RoutedEventArgs e)
        {
            if (diasave.ShowDialog() == true)
            {
                Heroius.Files.EntitySilo silo = new Heroius.Files.EntitySilo();
                silo.AutoStore = true;
                silo.Store("host", EVContext);
                //递归silo属性，注意，此语句位置敏感，必须在host操作之后
                silo.Recursion = true;
                silo.Store("container", EVContext.GetSelectedContainer());
                silo.Save(diasave.FileName);
            }
        }

    }

    /// <summary>
    /// 列举可用有效容积计算容器
    /// </summary>
    public enum EffectVolContainerType
    {
        /// <summary>
        /// 简单柱形容器
        /// </summary>
        [Description("简单柱形容器")]
        CC,
        /// <summary>
        /// 简单方形容器
        /// </summary>
        [Description("简单方形容器")]
        CR,
        /// <summary>
        /// 带有灰斗的柱形容器
        /// </summary>
        [Description("带有灰斗的柱形容器")]
        CH,
        /// <summary>
        /// 带有圆形下料口灰斗的方形容器
        /// </summary>
        [Description("带有圆形下料口灰斗的方形容器")]
        RHC,
        /// <summary>
        /// 带有矩形下料口灰斗的方形容器
        /// </summary>
        [Description("带有矩形下料口灰斗的方形容器")]
        RHS,
        /// <summary>
        /// 圆形下料口的方形立式布袋除尘器
        /// </summary>
        [Description("圆形下料口的方形布袋除尘器")]
        VRBC,
        /// <summary>
        /// 矩形下料口的方形立式布袋除尘器
        /// </summary>
        [Description("矩形下料口的方形布袋除尘器")]
        VRBR,
        /// <summary>
        /// 柱形立式布袋除尘器
        /// </summary>
        [Description("柱形布袋除尘器")]
        VCB
        ///// <summary>
        ///// 横插式布袋除尘器
        ///// </summary>
        //[Description("圆形下料口的横插式布袋除尘器")]
        //HBC,
        ///// <summary>
        ///// 矩形下料口的横插式布袋除尘器
        ///// </summary>
        //[Description("矩形下料口的横插式布袋除尘器")]
        //HBR
    }

    /// <summary>
    /// 在分层中表示模型的类
    /// </summary>
    public class EffectVolContext : ObservableEntity
    {
        /// <summary>
        /// 静态构造：初始化静态单件
        /// </summary>
        static EffectVolContext()
        {
            Context = new EffectVolContext();

        }
        /// <summary>
        /// 静态单件，出于经济考虑设计
        /// </summary>
        public static EffectVolContext Context { get; private set; }

        /// <summary>
        /// 私有构造函数：仅允许单件
        /// </summary>
        private EffectVolContext() { }

        /// <summary>
        /// 当前选中的容器类型
        /// </summary>
        public EffectVolContainerType SelectedType { get { return _SelectedType; } set { _SelectedType = value; RaisePropertyChangedEvent("SelectedType"); } }
        EffectVolContainerType _SelectedType = EffectVolContainerType.CC;

        #region Containers

        public CylinderContainer OBJ_CC { get { if (_OBJ_CC == null) _OBJ_CC = new CylinderContainer(); return _OBJ_CC; } }
        CylinderContainer _OBJ_CC = null;

        public BoxContainer OBJ_CR { get { if (_OBJ_CR == null) _OBJ_CR = new BoxContainer(); return _OBJ_CR; } }
        BoxContainer _OBJ_CR = null;

        public CylinderHopper OBJ_CH { get { if (_OBJ_CH == null) _OBJ_CH = new CylinderHopper(); return _OBJ_CH; } }
        CylinderHopper _OBJ_CH = null;

        public BoxCircleHopper OBJ_RHC { get { if (_OBJ_RHC == null) _OBJ_RHC = new BoxCircleHopper(); return _OBJ_RHC; } }
        BoxCircleHopper _OBJ_RHC = null;

        public BoxSquareHopper OBJ_RHS { get { if (_OBJ_RHS == null) _OBJ_RHS = new BoxSquareHopper(); return _OBJ_RHS; } }
        BoxSquareHopper _OBJ_RHS = null;

        public BoxCircleBagFilter OBJ_VRBC { get { if (_OBJ_VRBC == null) _OBJ_VRBC = new BoxCircleBagFilter(); return _OBJ_VRBC; } }
        BoxCircleBagFilter _OBJ_VRBC = null;

        public BoxSquareBagFilter OBJ_VRBR { get { if (_OBJ_VRBR == null) _OBJ_VRBR = new BoxSquareBagFilter(); return _OBJ_VRBR; } }
        BoxSquareBagFilter _OBJ_VRBR = null;

        public CylinderBagFilter OBJ_VCB { get { if (_OBJ_VCB == null) _OBJ_VCB = new CylinderBagFilter(); return _OBJ_VCB; } }
        CylinderBagFilter _OBJ_VCB = null;

        //public HorizontalBagFilter OBJ_HBC { get { if (_OBJ_HBC == null) _OBJ_HBC = new HorizontalBagFilter(SectionType.Circle); return _OBJ_HBC; } }
        //HorizontalBagFilter _OBJ_HBC = null;

        //public HorizontalBagFilter OBJ_HBR { get { if (_OBJ_HBR == null) _OBJ_HBR = new HorizontalBagFilter(SectionType.Square); return _OBJ_HBR; } }
        //HorizontalBagFilter _OBJ_HBR = null;

        /// <summary>
        /// 通过容器类型获取容器实例缓存
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public EPE.Vent.EffectVol.IContainer GetContainerByType(EffectVolContainerType type)
        {
            switch (type)
            {
                default:
                case EffectVolContainerType.CC: return OBJ_CC;
                case EffectVolContainerType.CR: return OBJ_CR;
                case EffectVolContainerType.CH: return OBJ_CH;
                case EffectVolContainerType.RHC: return OBJ_RHC;
                case EffectVolContainerType.RHS: return OBJ_RHS;
                case EffectVolContainerType.VRBC: return OBJ_VRBC;
                case EffectVolContainerType.VRBR: return OBJ_VRBR;
                case EffectVolContainerType.VCB: return OBJ_VCB;
                //case EffectVolContainerType.HBC: return OBJ_HBC;
                //case EffectVolContainerType.HBR: return OBJ_HBR;
            }
        }
        /// <summary>
        /// 通过当前选中类型获取容器实例缓存
        /// </summary>
        /// <returns></returns>
        public EPE.Vent.EffectVol.IContainer GetSelectedContainer()
        {
            return GetContainerByType(SelectedType);
        }

        #endregion
    }
}
